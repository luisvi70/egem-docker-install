#!/bin/sh
set -e

# TeamEGEM Notes:
# The following variables are used as part of the Egem node
# installation process.
docker_image='egemofficial/egemqn:080821'
ideal_ram='2048'
swapfile='/swapfile'

# This script is meant for quick & easy install via:
#   $ curl -fsSL https://get.docker.com -o get-docker.sh
#   $ sh get-docker.sh
#
# For test builds (ie. release candidates):
#   $ curl -fsSL https://test.docker.com -o test-docker.sh
#   $ sh test-docker.sh
#
# NOTE: Make sure to verify the contents of the script
#       you downloaded matches the contents of install.sh
#       located at https://github.com/docker/docker-install
#       before executing.
#
# Git commit from https://github.com/docker/docker-install when
# the script was uploaded (Should only be modified by upload job):
SCRIPT_COMMIT_SHA="${LOAD_SCRIPT_COMMIT_SHA}"


# The channel to install from:
#   * nightly
#   * test
#   * stable
#   * edge (deprecated)
DEFAULT_CHANNEL_VALUE="stable"
if [ -z "$CHANNEL" ]; then
	CHANNEL=$DEFAULT_CHANNEL_VALUE
fi

DEFAULT_DOWNLOAD_URL="https://download.docker.com"
if [ -z "$DOWNLOAD_URL" ]; then
	DOWNLOAD_URL=$DEFAULT_DOWNLOAD_URL
fi

DEFAULT_REPO_FILE="docker-ce.repo"
if [ -z "$REPO_FILE" ]; then
	REPO_FILE="$DEFAULT_REPO_FILE"
fi

mirror=''
DRY_RUN=${DRY_RUN:-}
while [ $# -gt 0 ]; do
	case "$1" in
		--mirror)
			mirror="$2"
			shift
			;;
		--dry-run)
			DRY_RUN=1
			;;
		--*)
			echo "Illegal option $1"
			;;
	esac
	shift $(( $# > 0 ? 1 : 0 ))
done

case "$mirror" in
	Aliyun)
		DOWNLOAD_URL="https://mirrors.aliyun.com/docker-ce"
		;;
	AzureChinaCloud)
		DOWNLOAD_URL="https://mirror.azure.cn/docker-ce"
		;;
esac

command_exists() {
	command -v "$@" > /dev/null 2>&1
}

sh_c='sh -c'
if [ "$user" != 'root' ]; then
	if command_exists sudo; then
		sh_c='sudo -E sh -c'
	elif command_exists su; then
		sh_c='su -c'
	else
		cat >&2 <<-'EOF'
		Error: this installer needs the ability to run commands as root.
		We are unable to find either "sudo" or "su" available to make this happen.
		EOF
		exit 1
	fi
fi

is_dry_run() {
	if [ -z "$DRY_RUN" ]; then
		return 1
	else
		return 0
	fi
}

deprecation_notice() {
	distro=$1
	date=$2
	echo
	echo "DEPRECATION WARNING:"
	echo "    The distribution, $distro, will no longer be supported in this script as of $date."
	echo "    If you feel this is a mistake please submit an issue at https://github.com/docker/docker-install/issues/new"
	echo
	sleep 10
}

get_distribution() {
	lsb_dist=""
	# Every system that we officially support has /etc/os-release
	if [ -r /etc/os-release ]; then
		lsb_dist="$(. /etc/os-release && echo "$ID")"
	fi
	# Returning an empty string here should be alright since the
	# case statements don't act unless you provide an actual value
	echo "$lsb_dist"
}

add_debian_backport_repo() {
	debian_version="$1"
	backports="deb http://ftp.debian.org/debian $debian_version-backports main"
	if ! grep -Fxq "$backports" /etc/apt/sources.list; then
		(set -x; $sh_c "echo \"$backports\" >> /etc/apt/sources.list")
	fi
}

echo_docker_as_nonroot() {
	if is_dry_run; then
		return
	fi
	if command_exists docker && [ -e /var/run/docker.sock ]; then
		(
			set -x
			$sh_c 'docker version'
		) || true
	fi
	your_user=your-user
	[ "$user" != 'root' ] && your_user="$user"
	# intentionally mixed spaces and tabs here -- tabs are stripped by "<<-EOF", spaces are kept in the output
	echo "If you would like to use Docker as a non-root user, you should now consider"
	echo "adding your user to the \"docker\" group with something like:"
	echo
	echo "  sudo usermod -aG docker $your_user"
	echo
	echo "Remember that you will have to log out and back in for this to take effect!"
	echo
	echo "WARNING: Adding a user to the \"docker\" group will grant the ability to run"
	echo "         containers which can be used to obtain root privileges on the"
	echo "         docker host."
	echo "         Refer to https://docs.docker.com/engine/security/security/#docker-daemon-attack-surface"
	echo "         for more information."

}

# Check if this is a forked Linux distro
check_forked() {

	# Check for lsb_release command existence, it usually exists in forked distros
	if command_exists lsb_release; then
		# Check if the `-u` option is supported
		set +e
		lsb_release -a -u > /dev/null 2>&1
		lsb_release_exit_code=$?
		set -e

		# Check if the command has exited successfully, it means we're in a forked distro
		if [ "$lsb_release_exit_code" = "0" ]; then
			# Print info about current distro
			cat <<-EOF
			You're using '$lsb_dist' version '$dist_version'.
			EOF

			# Get the upstream release info
			lsb_dist=$(lsb_release -a -u 2>&1 | tr '[:upper:]' '[:lower:]' | grep -E 'id' | cut -d ':' -f 2 | tr -d '[:space:]')
			dist_version=$(lsb_release -a -u 2>&1 | tr '[:upper:]' '[:lower:]' | grep -E 'codename' | cut -d ':' -f 2 | tr -d '[:space:]')

			# Print info about upstream distro
			cat <<-EOF
			Upstream release is '$lsb_dist' version '$dist_version'.
			EOF
		else
			if [ -r /etc/debian_version ] && [ "$lsb_dist" != "ubuntu" ] && [ "$lsb_dist" != "raspbian" ]; then
				if [ "$lsb_dist" = "osmc" ]; then
					# OSMC runs Raspbian
					lsb_dist=raspbian
				else
					# We're Debian and don't even know it!
					lsb_dist=debian
				fi
				dist_version="$(sed 's/\/.*//' /etc/debian_version | sed 's/\..*//')"
				case "$dist_version" in
					10)
						dist_version="buster"
					;;
					9)
						dist_version="stretch"
					;;
					8|'Kali Linux 2')
						dist_version="jessie"
					;;
				esac
			fi
		fi
	fi
}

semverParse() {
	major="${1%%.*}"
	minor="${1#$major.}"
	minor="${minor%%.*}"
	patch="${1#$major.$minor.}"
	patch="${patch%%[-.]*}"
}

ee_notice() {
	echo
	echo
	echo "  WARNING: $1 is now only supported by Docker EE"
	echo "           Check https://store.docker.com for information on Docker EE"
	echo
	echo
}

do_install_docker() {
	header "do_install_docker() - install docker in host system"
	subheader "# Executing docker install script, commit: $SCRIPT_COMMIT_SHA"

	if command_exists docker; then
		docker_version="$(docker -v | cut -d ' ' -f3 | cut -d ',' -f1)"
		MAJOR_W=1
		MINOR_W=10

		semverParse "$docker_version"

		shouldWarn=0
		if [ "$major" -lt "$MAJOR_W" ]; then
			shouldWarn=1
		fi

		if [ "$major" -le "$MAJOR_W" ] && [ "$minor" -lt "$MINOR_W" ]; then
			shouldWarn=1
		fi

		cat >&2 <<-'EOF'
			Warning: the "docker" command appears to already exist on this system.

			If you already have Docker installed, this script can cause trouble, which is
			why we're displaying this warning and provide the opportunity to cancel the
			installation.

			If you installed the current Docker package using this script and are using it
		EOF

		if [ $shouldWarn -eq 1 ]; then
			cat >&2 <<-'EOF'
			again to update Docker, we urge you to migrate your image store before upgrading
			to v1.10+.

			You can find instructions for this here:
			https://github.com/docker/docker/wiki/Engine-v1.10.0-content-addressability-migration
			EOF
		else
			cat >&2 <<-'EOF'
			again to update Docker, you can safely ignore this message.
			EOF
		fi

		cat >&2 <<-'EOF'

			You may press Ctrl+C now to abort this script.
		EOF
		( set -x; sleep 20 )
	fi

	user="$(id -un 2>/dev/null || true)"

	if is_dry_run; then
		sh_c="echo"
	fi

	# perform some very rudimentary platform detection
	lsb_dist=$( get_distribution )
	lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"

	case "$lsb_dist" in

		ubuntu)
			if command_exists lsb_release; then
				dist_version="$(lsb_release --codename | cut -f2)"
			fi
			if [ -z "$dist_version" ] && [ -r /etc/lsb-release ]; then
				dist_version="$(. /etc/lsb-release && echo "$DISTRIB_CODENAME")"
			fi
		;;

		debian|raspbian)
			dist_version="$(sed 's/\/.*//' /etc/debian_version | sed 's/\..*//')"
			case "$dist_version" in
				10)
					dist_version="buster"
				;;
				9)
					dist_version="stretch"
				;;
				8)
					dist_version="jessie"
				;;
			esac
		;;

		centos)
			if [ -z "$dist_version" ] && [ -r /etc/os-release ]; then
				dist_version="$(. /etc/os-release && echo "$VERSION_ID")"
			fi
		;;

		rhel|ol|sles)
			ee_notice "$lsb_dist"
			exit 1
			;;

		*)
			if command_exists lsb_release; then
				dist_version="$(lsb_release --release | cut -f2)"
			fi
			if [ -z "$dist_version" ] && [ -r /etc/os-release ]; then
				dist_version="$(. /etc/os-release && echo "$VERSION_ID")"
			fi
		;;

	esac

	# Check if this is a forked Linux distro
	check_forked

	# Run setup for each distro accordingly
	case "$lsb_dist" in
		ubuntu|debian|raspbian)
			pre_reqs="apt-transport-https ca-certificates curl"
			if [ "$lsb_dist" = "debian" ]; then
				# libseccomp2 does not exist for debian jessie main repos for aarch64
				if [ "$(uname -m)" = "aarch64" ] && [ "$dist_version" = "jessie" ]; then
					add_debian_backport_repo "$dist_version"
				fi
			fi

			if ! command -v gpg > /dev/null; then
				pre_reqs="$pre_reqs gnupg"
			fi
			apt_repo="deb [arch=$(dpkg --print-architecture)] $DOWNLOAD_URL/linux/$lsb_dist $dist_version $CHANNEL"
			(
				if ! is_dry_run; then
					set -x
				fi
				$sh_c 'apt-get update -qq >/dev/null'
				$sh_c "DEBIAN_FRONTEND=noninteractive apt-get install -y -qq $pre_reqs >/dev/null"
				$sh_c "curl -fsSL \"$DOWNLOAD_URL/linux/$lsb_dist/gpg\" | apt-key add -qq - >/dev/null"
				$sh_c "echo \"$apt_repo\" > /etc/apt/sources.list.d/docker.list"
				$sh_c 'apt-get update -qq >/dev/null'
			)
			pkg_version=""
			if [ -n "$VERSION" ]; then
				if is_dry_run; then
					echo "# WARNING: VERSION pinning is not supported in DRY_RUN"
				else
					# Will work for incomplete versions IE (17.12), but may not actually grab the "latest" if in the test channel
					pkg_pattern="$(echo "$VERSION" | sed "s/-ce-/~ce~.*/g" | sed "s/-/.*/g").*-0~$lsb_dist"
					search_command="apt-cache madison 'docker-ce' | grep '$pkg_pattern' | head -1 | awk '{\$1=\$1};1' | cut -d' ' -f 3"
					pkg_version="$($sh_c "$search_command")"
					echo "INFO: Searching repository for VERSION '$VERSION'"
					echo "INFO: $search_command"
					if [ -z "$pkg_version" ]; then
						echo
						echo "ERROR: '$VERSION' not found amongst apt-cache madison results"
						echo
						exit 1
					fi
					search_command="apt-cache madison 'docker-ce-cli' | grep '$pkg_pattern' | head -1 | awk '{\$1=\$1};1' | cut -d' ' -f 3"
					# Don't insert an = for cli_pkg_version, we'll just include it later
					cli_pkg_version="$($sh_c "$search_command")"
					pkg_version="=$pkg_version"
				fi
			fi
			(
				if ! is_dry_run; then
					set -x
				fi
				if [ -n "$cli_pkg_version" ]; then
					$sh_c "apt-get install -y -qq --no-install-recommends docker-ce-cli=$cli_pkg_version >/dev/null"
				fi
				$sh_c "apt-get install -y -qq --no-install-recommends docker-ce$pkg_version >/dev/null"
			)
			# echo_docker_as_nonroot
			;;
		centos|fedora)
			yum_repo="$DOWNLOAD_URL/linux/$lsb_dist/$REPO_FILE"
			if ! curl -Ifs "$yum_repo" > /dev/null; then
				echo "Error: Unable to curl repository file $yum_repo, is it valid?"
				exit 1
			fi
			if [ "$lsb_dist" = "fedora" ]; then
				pkg_manager="dnf"
				config_manager="dnf config-manager"
				enable_channel_flag="--set-enabled"
				disable_channel_flag="--set-disabled"
				pre_reqs="dnf-plugins-core"
				pkg_suffix="fc$dist_version"
			else
				pkg_manager="yum"
				config_manager="yum-config-manager"
				enable_channel_flag="--enable"
				disable_channel_flag="--disable"
				pre_reqs="yum-utils"
				pkg_suffix="el"
			fi
			(
				if ! is_dry_run; then
					set -x
				fi
				$sh_c "$pkg_manager install -y -q $pre_reqs"
				$sh_c "$config_manager --add-repo $yum_repo"

				if [ "$CHANNEL" != "stable" ]; then
					$sh_c "$config_manager $disable_channel_flag docker-ce-*"
					$sh_c "$config_manager $enable_channel_flag docker-ce-$CHANNEL"
				fi
				$sh_c "$pkg_manager makecache"
			)
			pkg_version=""
			if [ -n "$VERSION" ]; then
				if is_dry_run; then
					echo "# WARNING: VERSION pinning is not supported in DRY_RUN"
				else
					pkg_pattern="$(echo "$VERSION" | sed "s/-ce-/\\\\.ce.*/g" | sed "s/-/.*/g").*$pkg_suffix"
					search_command="$pkg_manager list --showduplicates 'docker-ce' | grep '$pkg_pattern' | tail -1 | awk '{print \$2}'"
					pkg_version="$($sh_c "$search_command")"
					echo "INFO: Searching repository for VERSION '$VERSION'"
					echo "INFO: $search_command"
					if [ -z "$pkg_version" ]; then
						echo
						echo "ERROR: '$VERSION' not found amongst $pkg_manager list results"
						echo
						exit 1
					fi
					search_command="$pkg_manager list --showduplicates 'docker-ce-cli' | grep '$pkg_pattern' | tail -1 | awk '{print \$2}'"
					# It's okay for cli_pkg_version to be blank, since older versions don't support a cli package
					cli_pkg_version="$($sh_c "$search_command" | cut -d':' -f 2)"
					# Cut out the epoch and prefix with a '-'
					pkg_version="-$(echo "$pkg_version" | cut -d':' -f 2)"
				fi
			fi
			(
				if ! is_dry_run; then
					set -x
				fi
				# install the correct cli version first
				if [ -n "$cli_pkg_version" ]; then
					$sh_c "$pkg_manager install -y -q docker-ce-cli-$cli_pkg_version"
				fi
				$sh_c "$pkg_manager install -y -q docker-ce$pkg_version"
			)
			# echo_docker_as_nonroot
			;;
		*)
			echo
			echo "ERROR: Unsupported distribution '$lsb_dist'"
			echo
			exit 1
			;;
	esac
}

do_install_egem_node() {
	header "do_install_egem_node() - install egem-node Docker image and run it in detached mode."

	# Lets verify docker is available in host system
	if ! [ -x "$(command -v docker)" ]; then
  	error 'do_install_egem_node() - docker is not installed.' >&2
  	exit 1
	fi

	# Lets verify the docker daemon is running
	# perform some very rudimentary platform detection
	lsb_dist=$( get_distribution )
	lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"

	case "$lsb_dist" in

		ubuntu)
			subheader 'Ubuntu system - No additional action for docker setup'
		;;

		debian|raspbian)
			subheader 'Debian/Raspbian system - No additional action for docker setup'
		;;

		centos)
			subheader 'Centos system - We need to start Docker daemon'
			$sh_c 'systemctl start docker'
			$sh_c 'systemctl enable docker'
		;;

		*)
			error "do_install_egem_node() - Unsupported distribution '$lsb_dist'"
			exit 1
		;;

	esac

	# Start (detached mode) egem-node Docker image
	subheader "start docker container image $docker_image"
	$sh_c "docker run --name egem-node -dit -p 8895:8895 -p 8897:8897 -p 30666:30666 --restart always $docker_image"
}

create_swapfile(){
		header "create_swapfile() - Setup swap space for host"

    # detect swap size
    current_swap=`swapon -se | grep -vi 'size' | awk '{s+=$3}END{print s}'`

    if [ -z "${current_swap}" ]; then
				subheader "${YELLOW}No swap space detected"
        current_swap="0"
    fi

    if [ "${current_swap}" -gt 0 ]; then
        current_swap=$((${current_swap}/1024))
    fi

    # display RAM/swap info
		total_ram=`free -m | grep -i "mem:" | awk '{print $2}'`
    subheader "total RAM size  : ${YELLOW}${total_ram} MB"
    subheader "total SWAP size : ${YELLOW}${current_swap} MB"

    # calculate necessary swap size
    missing_swap="$((${ideal_ram}-(${total_ram}+${current_swap})))"
		subheader "missing Swap space: ${YELLOW}${missing_swap} MB"

    if [ "${missing_swap}" -lt 100 ]; then
        missing_swap="0"
    fi

    if [ "${missing_swap}" -gt 0 ]; then
        subheader "creating a ${YELLOW}${missing_swap} MB${MAGENTA} swap file..."

				if [ -e "$swapfile" ]; then
        	$sh_c "swapoff -v ${swapfile}"
        	$sh_c "rm -rf ${swapfile}"
				fi

        $sh_c "dd if=/dev/zero of=${swapfile} bs=1MiB count=${missing_swap}" || error "create swap file (dd command)"

        $sh_c "chmod 600 ${swapfile}"
        $sh_c "mkswap ${swapfile}" || error "create swap file (mkswap)"
				sleep 5

        subheader "activating new swap file..."
        # swapon ${swapfile} &>/dev/null || error "activate swap file (swapon)"
        $sh_c "swapon ${swapfile}"

        # if script couldn't activate swap file, check "total ram + total swap" size
        # continue if it is greater or equal than 900 MB, halt if it is less than 900 MB
        if [ $? != "0" ]; then
            if [ "$((${total_ram}+${current_swap}))" -lt 900 ]; then
                error "create_swapfile() - failed to activate swap file"
            fi
        fi

				# Update /etc/fstab file to activate swapfile on system boot
        $sh_c "echo \"$(grep -v ${swapfile} /etc/fstab)\" > /etc/fstab"
        $sh_c "echo \"${swapfile} none swap sw 0 0\" >> /etc/fstab"

        $sh_c "swapon -a"
    else
        subheader "skipping swap file creation (you have enough RAM/Swap)..."

        return 0
    fi
}

do_uninstall_egem(){
	header "do_uninstall_egem() - Remove any previously installed Egem node container or images"

	# Verify if there is an Egem node container running, if so then stop it
	egem_container=`sudo docker ps -a | grep egem | awk '{ print $1; }'`
	if [ "${egem_container}" != "" ]; then
		subheader "removing Egem docker container running with ID: ${YELLOW}${egem_container}"

		# Lets remove the current Egem node container
		$sh_c "docker rm -f ${egem_container}"
		if [ $? != "0" ]; then
			error "do_uninstall_egem() - Failed to remove Egem docker container."
		fi
	fi

	# Verify if there is an Egem node image; if so then remove it.
	egem_image=`sudo docker images | grep egem | awk '{ print $3; }'`
	if [ "${egem_image}" != "" ]; then
		detected_egem_node="1"
		subheader "removing Egem docker image with ID: ${YELLOW}${egem_image}"

		# Lets remove the current Egem image
		$sh_c "docker rmi ${egem_image}"
		if [ $? != "0" ]; then
			error "do_uninstall_egem() - Failed to remove Egem docker image."
		fi
	fi
}

show_next_steps(){
    node_ip=`wget -q --no-check-certificate -O - ipinfo.io/ip`

    sleep 0.25s

		header "show_next_steps() - Please follow these instructions"

		if [ "${detected_egem_node}" = "1" ]; then
			echo ${YELLOW}"We detected a previous Egem node installation. So probably no need to register this node."
		else
    	echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    	echo ${GREEN}"EGEM Quarry Node setup has finished succesfully!"${NORMAL}
    	echo ${YELLOW}"Please follow these steps to activate your node payments:"${NORMAL}
    	echo
    	echo ${MAGENTA}"  1) Make sure you have the necessary balance in your wallet"${NORMAL}
    	echo ${CYAN}"      If this is a 10k (Tier 1) node  ->  at least 10k EGEM"${NORMAL}
    	echo ${CYAN}"      If this is a 30k (Tier 2) node  ->  at least 40k EGEM"${NORMAL}
    	echo
    	echo ${MAGENTA}"  2) Direct message 'The EGEM Master' bot these commands"${NORMAL}
    	echo ${CYAN}"      If this is your first 10k (Tier 1) node:"${NORMAL}
    	echo ${YELLOW}"          /register"${NORMAL}
    	echo ${YELLOW}"          /setaddress YourWalletAddress"${NORMAL}
    	echo ${YELLOW}"          /an 1 1 ${node_ip}"${NORMAL}
    	echo
    	echo ${CYAN}"      If this is your first 30k (Tier 2) node:"${NORMAL}
    	echo ${YELLOW}"          /an 2 1 ${node_ip}"${NORMAL}
    	echo
    	echo ${CYAN}"Check ${YELLOW}wiki.egem.io/qnregister${CYAN} for 3 or more nodes on same account."${NORMAL}
    	echo
    	echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
		fi

    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${YELLOW}"You can check if the Egem node container is running, by executing:"${NORMAL}
    echo ${GREEN}"      $ sudo docker ps -a"${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}

    sleep 3s
}

early_header(){
    text="${1}"

    echo ${GREEN}"-------------------------------------------------------------------"
    echo "${WHITE}[${GREEN}*${WHITE}] ${CYAN}${text}${NORMAL}"
}

header(){
    text="${1}"

    echo ${GREEN}"-------------------------------------------------------------------"
    echo "${WHITE}[${GREEN}${step}${WHITE}] ${CYAN}${text}${NORMAL}"

    step=$(( step + 1))
}

subheader(){
    text="${1}"

    echo "    ${WHITE}> ${MAGENTA}${text}${NORMAL}"
}

subtext(){
    text="${1}"

    echo "      ${GREEN}- ${CYAN}${text}${NORMAL}"
}

error(){
    failed_step="${1}"

    echo ${RED}"Error! Install failed at this step ->  ${YELLOW}${failed_step}"${NORMAL}
    echo ${RED}"Please re-run the script. Contact developers if it happens again."${NORMAL}
    exit 1
}

# -----------------------------------------------------------------
# color codes for echo commands
NORMAL='\e[0m'
RED='\e[31;01m'
GREEN='\e[32;01m'
YELLOW='\e[33;01m'
BLUE='\e[34;01m'
MAGENTA='\e[35;01m'
CYAN='\e[36;01m'
WHITE='\e[97;01m'
# -----------------------------------------------------------------

# Initialize some variables
step="1"
detected_egem_node="0"

# detect swap size
header "detect swap space in host"
detect_swap=`swapon -se | grep -vi 'size' | awk '{s+=$3}END{print s}'`

if [ -z "${detect_swap}" ]; then
		subheader "${GREEN}no swap space detected"
		create_swapfile
		sleep 5
	else
		subheader "${YELLOW}${detect_swap} KB swap space detected in this system"
fi

if ! command_exists docker; then
	do_install_docker
else
	# Verify if we need to remove any previous Egem container/images.
	do_uninstall_egem
fi

do_install_egem_node

show_next_steps
