# Minimum Hardware Requirements

- CPU: 1 core
- RAM: 2 GB total between RAM + swap
- Disk: 20 GB HDD/SDD
- Bandwidth: 250 GB per month
- Operating System: Ubuntu 18/16, CentOS 7, Debian 9 or Raspbian 9

# Installation Steps

#### NOTE: The installation of this Egem docker container image replaces previous versions of an installed EGEM node in your host.

#### 1) Create a VPS

  - TEAM EGEM Referral links:

    **Pecunia: [`https://pecuniaplatform.io/ref/c8d7dee0`](https://pecuniaplatform.io/ref/c8d7dee0)**

    **VirMach: [`https://billing.virmach.com/aff.php?aff=10243`](https://billing.virmach.com/aff.php?aff=10243)**

    **Vultr: [`https://www.vultr.com/?ref=7408289`](https://www.vultr.com/?ref=7408289)**

    **Linode: [`https://www.linode.com/?r=0543b2c292a0dcae51ac3fea3d7f170d956565c3`](https://www.linode.com/?r=0543b2c292a0dcae51ac3fea3d7f170d956565c3)**

#### 2) Get console access to your VPS

  - Use console/terminal or a tool like **[`Putty`](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)**

  - Login directly as **`root`**

    **IMPORTANT NOTE:**

    If you need to work on a non-root user, you must run **`sudo -s`** first, otherwise setup might not work properly

#### 3) Run the following command on your VPS console to install Docker + Egem node
To install both Docker and the Egem node image execute the following command:
  > **`sh -c "$(curl -ko - https://gitlab.com/luisvi70/egem-docker-install/raw/master/install.sh)"`**

  or use this one if your system doesn't have **`curl`** installed:

  > **`sh -c "$(wget --no-check-certificate -O - https://gitlab.com/luisvi70/egem-docker-install/raw/master/install.sh)"`**

After successful installation of Docker + Egem node, you can run the following command to make sure the Egem node container image is running:

  > **`sudo docker ps -a`**

#### 4) Register and activate your Quarry Node.

Follow the Quarry Node registration instructions at [`https://wiki.egem.io/qnregister`](https://wiki.egem.io/qnregister)

#### Join The TEAM EGEM Conversation

  - Discord: [`https://discord.gg/zav3EkW`](https://discord.gg/zav3EkW)

  - Twitter: [`https://twitter.com/ethergemcoin`](https://twitter.com/ethergemcoin)

  - Reddit: [https://www.reddit.com/r/egem/](https://www.reddit.com/r/egem/)

  - Telegram: [`https://t.me/egemofficial`](https://t.me/egemofficial)

  - Bitcointalk: [`https://bitcointalk.org/index.php?topic=3167940.0`](https://bitcointalk.org/index.php?topic=3167940.0)
